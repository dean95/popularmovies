package com.dean.popularmovies.ui.dagger.application;

import com.dean.popularmovies.ui.dagger.application.module.ApplicationModule;
import com.dean.popularmovies.ui.dagger.application.module.DataModule;
import com.dean.popularmovies.ui.dagger.application.module.MappersModule;
import com.dean.popularmovies.ui.dagger.application.module.ThreadingModule;
import com.dean.popularmovies.ui.dagger.application.module.UtilsModule;

public interface ApplicationComponentExposes extends ApplicationModule.Exposes,
        ThreadingModule.Exposes,
        DataModule.Exposes,
        MappersModule.Exposes,
        UtilsModule.Exposes {
}
