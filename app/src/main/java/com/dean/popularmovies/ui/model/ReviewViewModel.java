package com.dean.popularmovies.ui.model;

public final class ReviewViewModel {

    private final String author;
    private final String content;

    public ReviewViewModel(String author, String content) {
        this.author = author;
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }

    public String getContent() {
        return content;
    }
}
