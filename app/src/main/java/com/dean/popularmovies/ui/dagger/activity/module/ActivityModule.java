package com.dean.popularmovies.ui.dagger.activity.module;

import android.app.Activity;
import android.content.Context;

import com.dean.popularmovies.ui.dagger.activity.ActivityScope;
import com.dean.popularmovies.ui.dagger.activity.DaggerActivity;
import com.dean.popularmovies.ui.dagger.activity.ForActivity;
import com.dean.popularmovies.ui.router.Router;
import com.dean.popularmovies.ui.router.RouterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    private final DaggerActivity daggerActivity;

    public ActivityModule(final DaggerActivity daggerActivity) {
        this.daggerActivity = daggerActivity;
    }

    @Provides
    @ActivityScope
    @ForActivity
    Context provideActivityContext() {
        return daggerActivity;
    }

    @Provides
    @ActivityScope
    Activity provideActivity() {
        return daggerActivity;
    }

    @Provides
    @ActivityScope
    Router provideRouter() {
        return new RouterImpl(daggerActivity);
    }

    public interface Exposes {

    }
}
