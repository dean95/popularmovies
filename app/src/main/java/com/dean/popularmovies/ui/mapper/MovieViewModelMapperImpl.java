package com.dean.popularmovies.ui.mapper;

import com.dean.popularmovies.domain.model.Movie;
import com.dean.popularmovies.domain.model.Review;
import com.dean.popularmovies.domain.model.Trailer;
import com.dean.popularmovies.ui.model.MovieViewModel;
import com.dean.popularmovies.ui.model.ReviewViewModel;
import com.dean.popularmovies.ui.model.TrailerViewModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public final class MovieViewModelMapperImpl implements MovieViewModelMapper {

    @Override
    public List<MovieViewModel> domainToViewModel(final List<Movie> moviesDomain) {
        final List<MovieViewModel> movieViewModels = new ArrayList<>(moviesDomain.size());

        for (final Movie movie : moviesDomain) {

            final MovieViewModel movieViewModel = new MovieViewModel(movie.getId(), movie.getPosterPath(), movie.getTitle(), formatDate(movie.getReleaseDate()),
                    movie.getVoteAverage(), movie.getOverview());
            movieViewModels.add(movieViewModel);
        }

        return movieViewModels;
    }

    @Override
    public List<TrailerViewModel> domainTrailerToViewModel(final List<Trailer> trailersDomain) {
        final List<TrailerViewModel> trailerViewModels = new ArrayList<>(trailersDomain.size());

        for (final Trailer trailer : trailersDomain) {
            final TrailerViewModel trailerViewModel = new TrailerViewModel(trailer.getName(), trailer.getKey());
            trailerViewModels.add(trailerViewModel);
        }

        return trailerViewModels;
    }

    @Override
    public List<ReviewViewModel> domainReviewToViewModel(final List<Review> reviewsDomain) {
        final List<ReviewViewModel> reviewViewModels = new ArrayList<>(reviewsDomain.size());

        for (final Review review : reviewsDomain) {
            final ReviewViewModel reviewViewModel = new ReviewViewModel(review.getAuthor(), review.getContent());
            reviewViewModels.add(reviewViewModel);
        }

        return reviewViewModels;
    }

    private String formatDate(final String stringDate) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date convertedDate = new Date();
        try {
            convertedDate = df.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        df = new SimpleDateFormat("MMM dd, yyyy", Locale.ENGLISH);
        return df.format(convertedDate);
    }
}
