package com.dean.popularmovies.ui.main;

import com.dean.popularmovies.domain.repository.MovieRepository;
import com.dean.popularmovies.ui.base.BasePresenter;
import com.dean.popularmovies.ui.mapper.MovieViewModelMapper;
import com.dean.popularmovies.ui.model.MovieViewModel;
import com.dean.popularmovies.ui.router.Router;

import java.util.List;

import javax.inject.Inject;

public final class MainPresenter extends BasePresenter<MainContract.View> implements MainContract.Presenter {

    @Inject
    MovieRepository movieRepository;

    @Inject
    MovieViewModelMapper viewModelMapper;

    @Inject
    Router router;

    public MainPresenter(final MainContract.View view) {
        super(view);
    }

    @Override
    public void getMovies() {
        addDisposable(movieRepository.getMovies()
                                     .map(viewModelMapper::domainToViewModel)
                                     .observeOn(mainScheduler)
                                     .subscribe(this::fetchMoviesSuccess,
                                                this::fetchMoviesFail));
    }

    @Override
    public void showPreferences() {
        router.showPreferenceScreen();
    }

    @Override
    public void showDetails(final MovieViewModel movieViewModel) {
        router.showDetailScreen(movieViewModel);
    }

    private void fetchMoviesSuccess(final List<MovieViewModel> movieViewModels) {
        getNullableView().renderMovies(movieViewModels);
    }

    private void fetchMoviesFail(final Throwable throwable) {
        getNullableView().movieFetchFailed();
    }
}
