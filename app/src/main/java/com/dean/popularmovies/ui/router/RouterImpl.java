package com.dean.popularmovies.ui.router;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import com.dean.popularmovies.ui.detail.DetailActivity;
import com.dean.popularmovies.ui.model.MovieViewModel;
import com.dean.popularmovies.ui.preferences.PreferencesActivity;

public final class RouterImpl implements Router {

    private static final String YOUTUBE_BASE_URI = "https://www.youtube.com/watch?v=";

    private final Activity activity;

    public RouterImpl(final Activity activity) {
        this.activity = activity;
    }

    @Override
    public void showPreferenceScreen() {
        final Intent intent = PreferencesActivity.createIntent(activity);
        activity.startActivity(intent);
    }

    @Override
    public void showDetailScreen(final MovieViewModel movieViewModel) {
        final Intent intent = DetailActivity.createIntent(activity);
        intent.putExtra(DetailActivity.MOVIE_EXTRA, movieViewModel);
        activity.startActivity(intent);
    }

    @Override
    public void showTrailer(String key) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(YOUTUBE_BASE_URI + key));
        activity.startActivity(intent);
    }
}
