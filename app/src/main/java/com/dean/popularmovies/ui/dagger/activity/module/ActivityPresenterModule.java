package com.dean.popularmovies.ui.dagger.activity.module;

import com.dean.popularmovies.ui.dagger.activity.ActivityScope;
import com.dean.popularmovies.ui.dagger.activity.DaggerActivity;
import com.dean.popularmovies.ui.detail.DetailContract;
import com.dean.popularmovies.ui.detail.DetailPresenter;
import com.dean.popularmovies.ui.main.MainContract;
import com.dean.popularmovies.ui.main.MainPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityPresenterModule {

    private final DaggerActivity daggerActivity;

    public ActivityPresenterModule(final DaggerActivity daggerActivity) {
        this.daggerActivity = daggerActivity;
    }

    @Provides
    @ActivityScope
    MainContract.Presenter provideMainPresenter() {
        final MainPresenter presenter = new MainPresenter((MainContract.View) daggerActivity);
        daggerActivity.getActivityComponent().inject(presenter);
        return presenter;
    }

    @Provides
    @ActivityScope
    DetailContract.Presenter provideDetailPresenter() {
        final DetailPresenter presenter = new DetailPresenter((DetailContract.View) daggerActivity);
        daggerActivity.getActivityComponent().inject(presenter);
        return presenter;
    }

    public interface Exposes {

    }
}
