package com.dean.popularmovies.ui.dagger.activity;

import com.dean.popularmovies.ui.detail.DetailActivity;
import com.dean.popularmovies.ui.detail.DetailPresenter;
import com.dean.popularmovies.ui.main.MainActivity;
import com.dean.popularmovies.ui.main.MainPresenter;

public interface ActivityComponentInjects {

    void inject(MainActivity mainActivity);
    void inject(MainPresenter mainPresenter);

    void inject(DetailActivity detailActivity);
    void inject(DetailPresenter detailPresenter);
}
