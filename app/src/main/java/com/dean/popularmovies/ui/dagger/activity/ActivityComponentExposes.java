package com.dean.popularmovies.ui.dagger.activity;

import com.dean.popularmovies.ui.dagger.activity.module.ActivityModule;
import com.dean.popularmovies.ui.dagger.activity.module.ActivityPresenterModule;

public interface ActivityComponentExposes extends ActivityModule.Exposes,
                                                  ActivityPresenterModule.Exposes {
}
