package com.dean.popularmovies.ui.base;

public interface ScopedPresenter {

    void start();

    void activate();

    void deactivate();

    void destroy();
}
