package com.dean.popularmovies.ui.model;

public final class TrailerViewModel {

    private final String name;
    private final String key;

    public TrailerViewModel(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }
}
