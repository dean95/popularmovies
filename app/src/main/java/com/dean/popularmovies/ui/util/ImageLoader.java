package com.dean.popularmovies.ui.util;

import android.widget.ImageView;

public interface ImageLoader {

    void loadImage(String url, ImageView targetView);
}
