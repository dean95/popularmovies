package com.dean.popularmovies.ui.router;

import com.dean.popularmovies.ui.model.MovieViewModel;

public interface Router {

    void showPreferenceScreen();

    void showDetailScreen(MovieViewModel movieViewModel);

    void showTrailer(String key);
}
