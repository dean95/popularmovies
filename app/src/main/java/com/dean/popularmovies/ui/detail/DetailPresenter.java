package com.dean.popularmovies.ui.detail;

import android.database.Cursor;

import com.dean.popularmovies.domain.repository.MovieRepository;
import com.dean.popularmovies.ui.base.BasePresenter;
import com.dean.popularmovies.ui.mapper.MovieViewModelMapper;
import com.dean.popularmovies.ui.model.ReviewViewModel;
import com.dean.popularmovies.ui.model.TrailerViewModel;
import com.dean.popularmovies.ui.router.Router;

import java.util.List;

import javax.inject.Inject;

public final class DetailPresenter extends BasePresenter<DetailContract.View> implements DetailContract.Presenter {

    @Inject
    MovieRepository movieRepository;

    @Inject
    MovieViewModelMapper movieViewModelMapper;

    @Inject
    Router router;

    public DetailPresenter(final DetailContract.View view) {
        super(view);
    }

    @Override
    public void getTrailers(final int id) {
        addDisposable(movieRepository.getTrailers(id)
                .map(movieViewModelMapper::domainTrailerToViewModel)
                .observeOn(mainScheduler)
                .subscribe(this::fetchTrailersSuccess,
                        this::fetchTrailersFail));
    }

    @Override
    public void getReviews(int id) {
        addDisposable(movieRepository.getReviews(id)
                .map(movieViewModelMapper::domainReviewToViewModel)
                .observeOn(mainScheduler)
                .subscribe(this::fetchReviewsSuccess,
                        this::fetchReviewsFail));
    }

    @Override
    public void addMovieToFavorite(int id, String title, String posterUrl, String overview,
                                   double rating, String releaseDate) {
        addDisposable(movieRepository.addMovieToFavorite(id, title, posterUrl, overview, rating, releaseDate)
                .observeOn(mainScheduler)
                .subscribe(this::addFavoriteSuccess,
                        this::addFavoriteFail));
    }

    @Override
    public void showTrailer(final String key) {
        router.showTrailer(key);
    }

    @Override
    public void isFavorite(int id) {
        addDisposable(movieRepository.isFavorite(id)
                .observeOn(mainScheduler)
                .subscribe(this::isFavoriteTrue));
    }

    @Override
    public void removeFromFavorites(int id) {
        addDisposable(movieRepository.removeFromFavorites(id)
                .observeOn(mainScheduler)
                .subscribe(this::favoriteRemovedSuccess,
                        this::favoriteRemoveFail));
    }

    private void favoriteRemovedSuccess() {
        getNullableView().favoriteRemoved();
    }

    private void favoriteRemoveFail(final Throwable throwable) {
        getNullableView().favoriteRemoveFail();
        throwable.printStackTrace();
    }

    private void isFavoriteTrue(final Cursor cursor) {
        if (cursor.getCount() > 0) {
            getNullableView().setFavoriteTrue();
        } else {
            getNullableView().setFavoriteFalse();
        }
    }

    private void addFavoriteSuccess() {
        getNullableView().addFavoriteSuccess();
    }

    private void addFavoriteFail(final Throwable throwable) {
        getNullableView().addFavoriteFail();
        throwable.printStackTrace();
    }

    private void fetchTrailersSuccess(final List<TrailerViewModel> trailerViewModel) {
        getNullableView().renderTrailers(trailerViewModel);
        getNullableView().hideEmptyTrailersView();
    }

    private void fetchTrailersFail(final Throwable throwable) {
        getNullableView().showEmptyTrailersView();
    }

    private void fetchReviewsSuccess(final List<ReviewViewModel> reviewViewModels) {
        getNullableView().renderReviews(reviewViewModels);
    }

    private void fetchReviewsFail(final Throwable throwable) {
        getNullableView().showEmptyReviewView();
    }
}
