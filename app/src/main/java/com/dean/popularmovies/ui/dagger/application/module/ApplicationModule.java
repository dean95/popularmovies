package com.dean.popularmovies.ui.dagger.application.module;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;

import com.dean.popularmovies.ui.dagger.application.ForApplication;
import com.dean.popularmovies.ui.dagger.application.PopularMoviesApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final PopularMoviesApplication application;

    public ApplicationModule(final PopularMoviesApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    PopularMoviesApplication providePopularMoviesApplication() {
        return application;
    }

    @Provides
    @Singleton
    @ForApplication
    Context provideApplicationContext() {
        return application;
    }

    @Provides
    @Singleton
    Resources provideResources() {
        return application.getResources();
    }

    @Provides
    @Singleton
    LayoutInflater provideLayoutInflater() {
        return (LayoutInflater) application.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Provides
    @Singleton
    ContentResolver provideContentResolver() {
        return application.getContentResolver();
    }

    public interface Exposes {

        PopularMoviesApplication popularMoviesApplication();

        @ForApplication
        Context context();

        Resources resources();

        LayoutInflater layoutInflater();

        ContentResolver contentResolver();
    }
}
