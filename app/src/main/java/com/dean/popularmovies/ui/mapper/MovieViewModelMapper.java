package com.dean.popularmovies.ui.mapper;

import com.dean.popularmovies.domain.model.Movie;
import com.dean.popularmovies.domain.model.Review;
import com.dean.popularmovies.domain.model.Trailer;
import com.dean.popularmovies.ui.model.MovieViewModel;
import com.dean.popularmovies.ui.model.ReviewViewModel;
import com.dean.popularmovies.ui.model.TrailerViewModel;

import java.util.List;

public interface MovieViewModelMapper {

    List<MovieViewModel> domainToViewModel(List<Movie> moviesDomain);

    List<TrailerViewModel> domainTrailerToViewModel(List<Trailer> trailersDomain);

    List<ReviewViewModel> domainReviewToViewModel(List<Review> reviewsDomain);
}
