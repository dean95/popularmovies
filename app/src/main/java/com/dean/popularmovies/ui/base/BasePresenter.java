package com.dean.popularmovies.ui.base;

import android.support.annotation.Nullable;

import com.dean.popularmovies.ui.dagger.application.module.ThreadingModule;

import java.lang.ref.WeakReference;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BasePresenter<View extends BaseView> implements ScopedPresenter {

    @Inject
    @Named(ThreadingModule.MAIN_SCHEDULER)
    protected Scheduler mainScheduler;

    private WeakReference<View> viewReference = new WeakReference<>(null);

    private CompositeDisposable disposables = new CompositeDisposable();

    public BasePresenter(final View view) {
        this.viewReference = new WeakReference<>(view);
    }

    @Override
    public void start() {

    }

    @Override
    public void activate() {

    }

    @Override
    public void deactivate() {
        disposables.clear();
    }

    @Override
    public void destroy() {

    }

    protected void addDisposable(final Disposable disposable) {
        if (disposables == null || disposables.isDisposed()) {
            disposables = new CompositeDisposable();
        }
        disposables.add(disposable);
    }

    @Nullable
    protected View getNullableView() {
        return viewReference.get();
    }
}
