package com.dean.popularmovies.ui.detail;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dean.popularmovies.R;
import com.dean.popularmovies.ui.model.TrailerViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;

public final class DetailTrailersAdapter extends RecyclerView.Adapter<DetailTrailersAdapter.TrailerViewHolder> {

    private static final long CLICK_THROTTLE_WINDOW_MILLIS = 300L;

    private final Subject<TrailerViewModel> onItemClickSubject = BehaviorSubject.create();

    private List<TrailerViewModel> trailerViewModels = new ArrayList<>();

    @Override
    public TrailerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trailer_item_view, parent, false);
        return new TrailerViewHolder(itemView, onItemClickSubject);
    }

    @Override
    public void onBindViewHolder(final TrailerViewHolder holder, final int position) {
        holder.setItem(trailerViewModels.get(position));
    }

    @Override
    public int getItemCount() {
        return trailerViewModels.size();
    }

    public void onTrailersUpdate(final List<TrailerViewModel> trailerViewModels) {
        this.trailerViewModels = trailerViewModels;
        notifyDataSetChanged();
    }

    public Observable<TrailerViewModel> onItemClick() {
        return onItemClickSubject.throttleFirst(CLICK_THROTTLE_WINDOW_MILLIS, TimeUnit.MILLISECONDS);
    }

    static final class TrailerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_detail_trailer_name)
        TextView trailerName;

        private final Subject<TrailerViewModel> clickSubject;

        private TrailerViewModel trailerViewModel;

        public TrailerViewHolder(final View itemView, final Subject<TrailerViewModel> clickSubject) {
            super(itemView);
            this.clickSubject = clickSubject;

            ButterKnife.bind(this, itemView);
        }

        public void setItem(final TrailerViewModel trailerViewModel) {
            this.trailerViewModel = trailerViewModel;
            trailerName.setText(trailerViewModel.getName());
        }

        @OnClick(R.id.iv_detail_trailer_play)
        void onPlayClick() {
            clickSubject.onNext(trailerViewModel);
        }
    }
}
