package com.dean.popularmovies.ui.main;

import com.dean.popularmovies.ui.base.BaseView;
import com.dean.popularmovies.ui.base.ScopedPresenter;
import com.dean.popularmovies.ui.model.MovieViewModel;

import java.util.List;

public interface MainContract {

    interface Presenter extends ScopedPresenter {

        void getMovies();

        void showPreferences();

        void showDetails(MovieViewModel movieViewModel);
    }

    interface View extends BaseView {

        void renderMovies(List<MovieViewModel> movieViewModels);

        void movieFetchFailed();
    }
}
