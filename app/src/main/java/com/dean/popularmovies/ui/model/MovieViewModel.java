package com.dean.popularmovies.ui.model;

import android.os.Parcel;
import android.os.Parcelable;

public final class MovieViewModel implements Parcelable {

    private final int id;
    private final String posterPath;
    private final String title;
    private final String releaseDate;
    private final double rating;
    private final String description;

    public MovieViewModel(final int id, final String posterPath, final String title, final String releaseDate,
                          final double rating, final String description) {
        this.id = id;
        this.posterPath = posterPath;
        this.title = title;
        this.releaseDate = releaseDate;
        this.rating = rating;
        this.description = description;
    }

    protected MovieViewModel(Parcel in) {
        id = in.readInt();
        posterPath = in.readString();
        title = in.readString();
        releaseDate = in.readString();
        rating = in.readDouble();
        description = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(posterPath);
        dest.writeString(title);
        dest.writeString(releaseDate);
        dest.writeDouble(rating);
        dest.writeString(description);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MovieViewModel> CREATOR = new Creator<MovieViewModel>() {

        @Override
        public MovieViewModel createFromParcel(Parcel in) {
            return new MovieViewModel(in);
        }

        @Override
        public MovieViewModel[] newArray(int size) {
            return new MovieViewModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public String getTitle() {
        return title;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public double getRating() {
        return rating;
    }

    public String getDescription() {
        return description;
    }
}
