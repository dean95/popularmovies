package com.dean.popularmovies.ui.dagger;

import com.dean.popularmovies.ui.dagger.activity.ActivityComponent;
import com.dean.popularmovies.ui.dagger.activity.DaggerActivity;
import com.dean.popularmovies.ui.dagger.application.ApplicationComponent;
import com.dean.popularmovies.ui.dagger.application.PopularMoviesApplication;

public final class ComponentFactory {

    private ComponentFactory() {
    }

    public static ApplicationComponent createApplicationComponent(final PopularMoviesApplication popularMoviesApplication) {
        return ApplicationComponent.Initializer.init(popularMoviesApplication);
    }

    public static ActivityComponent createActivityComponent(final DaggerActivity daggerActivity, final PopularMoviesApplication popularMoviesApplication) {
        return ActivityComponent.Initializer.init(daggerActivity, popularMoviesApplication.getApplicationComponent());
    }
}
