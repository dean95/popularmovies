package com.dean.popularmovies.ui.dagger.application;

import android.app.Application;

import com.dean.popularmovies.BuildConfig;
import com.dean.popularmovies.ui.dagger.ComponentFactory;
import com.facebook.stetho.Stetho;

public final class PopularMoviesApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = ComponentFactory.createApplicationComponent(this);
        applicationComponent.inject(this);

        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this);
        }
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
