package com.dean.popularmovies.ui.dagger.application.module;

import com.dean.popularmovies.data.movies.converter.MovieModelConverter;
import com.dean.popularmovies.data.movies.converter.MovieModelConverterImpl;
import com.dean.popularmovies.ui.mapper.MovieViewModelMapper;
import com.dean.popularmovies.ui.mapper.MovieViewModelMapperImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class MappersModule {

    @Provides
    @Singleton
    MovieModelConverter provideMovieModelConverter() {
        return new MovieModelConverterImpl();
    }

    @Provides
    @Singleton
    MovieViewModelMapper provideMovieViewModelMapper() {
        return new MovieViewModelMapperImpl();
    }

    public interface Exposes {

        MovieModelConverter movieModelConverter();

        MovieViewModelMapper movieViewModelMapper();
    }
}
