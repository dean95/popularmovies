package com.dean.popularmovies.ui.dagger.application.module;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;

import com.dean.popularmovies.data.movies.MovieRepositoryImpl;
import com.dean.popularmovies.data.movies.converter.MovieModelConverter;
import com.dean.popularmovies.data.movies.network.client.TheMovieDbApi;
import com.dean.popularmovies.data.movies.network.client.TheMovieDbClient;
import com.dean.popularmovies.data.movies.network.client.TheMovieDbClientImpl;
import com.dean.popularmovies.data.util.PreferenceUtils;
import com.dean.popularmovies.data.util.PreferenceUtilsImpl;
import com.dean.popularmovies.domain.repository.MovieRepository;
import com.dean.popularmovies.ui.dagger.application.ForApplication;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class DataModule {

    private static final String BASE_URL = "http://api.themoviedb.org/3/";

    @Provides
    @Singleton
    Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    TheMovieDbApi provideTheMovieDbApi(final Retrofit retrofit) {
        return retrofit.create(TheMovieDbApi.class);
    }

    @Provides
    @Singleton
    TheMovieDbClient provideTheMovieDbClient(final TheMovieDbApi theMovieDbApi) {
        return new TheMovieDbClientImpl(theMovieDbApi);
    }

    @Provides
    @Singleton
    MovieRepository provideMovieRepository(final TheMovieDbClient theMovieDbClient, final MovieModelConverter movieModelConverter,
                                           @Named(ThreadingModule.BACKGROUND_SCHEDULER) final Scheduler backgroundScheduler,
                                           final PreferenceUtils preferenceUtils, final ContentResolver contentResolver) {
        return new MovieRepositoryImpl(theMovieDbClient, movieModelConverter, backgroundScheduler, preferenceUtils, contentResolver);
    }

    @Provides
    @Singleton
    PreferenceUtils providePreferenceUtils(final @ForApplication Context context, final Resources resources) {
        return new PreferenceUtilsImpl(context, resources);
    }

    public interface Exposes {

        MovieRepository movieRepository();
    }
}
