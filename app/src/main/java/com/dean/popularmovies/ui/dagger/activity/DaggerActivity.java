package com.dean.popularmovies.ui.dagger.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.dean.popularmovies.ui.dagger.ComponentFactory;
import com.dean.popularmovies.ui.dagger.application.PopularMoviesApplication;

public abstract class DaggerActivity extends AppCompatActivity {

    private ActivityComponent activityComponent;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inject(getActivityComponent());
    }

    public ActivityComponent getActivityComponent() {
        if (activityComponent == null) {
            activityComponent = ComponentFactory.createActivityComponent(this, getPopularMoviesApplication());
        }
        return activityComponent;
    }

    private PopularMoviesApplication getPopularMoviesApplication() {
        return ((PopularMoviesApplication) getApplication());
    }

    protected abstract void inject(final ActivityComponent activityComponent);
}
