package com.dean.popularmovies.ui.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dean.popularmovies.R;
import com.dean.popularmovies.ui.model.MovieViewModel;
import com.dean.popularmovies.ui.util.ImageLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;

public final class MainAdapter extends RecyclerView.Adapter<MainAdapter.MovieViewHolder> {

    private static final long CLICK_THROTTLE_WINDOW_MILLIS = 300L;

    private final Subject<MovieViewModel> onItemClickSubject = BehaviorSubject.create();

    private final ImageLoader imageLoader;

    private List<MovieViewModel> viewModelList = new ArrayList<>();

    public MainAdapter(final ImageLoader imageLoader) {
        this.imageLoader = imageLoader;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                                            .inflate(R.layout.movie_poster_item_view, parent, false);
        return new MovieViewHolder(itemView, imageLoader, onItemClickSubject);
    }

    @Override
    public void onBindViewHolder(final MovieViewHolder holder, final int position) {
        holder.setItem(viewModelList.get(position));
    }

    @Override
    public int getItemCount() {
        return viewModelList.size();
    }

    public void onMoviesUpdate(final List<MovieViewModel> viewModelList) {
        this.viewModelList = viewModelList;
        notifyDataSetChanged();
    }

    public Observable<MovieViewModel> onItemClick() {
        return onItemClickSubject.throttleFirst(CLICK_THROTTLE_WINDOW_MILLIS, TimeUnit.MILLISECONDS);
    }

    static final class MovieViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_main_movie_poster)
        ImageView moviePoster;

        private final ImageLoader imageLoader;

        private final Subject<MovieViewModel> clickSubject;

        private MovieViewModel movieViewModel;

        public MovieViewHolder(final View itemView, final ImageLoader imageLoader,
                               final Subject<MovieViewModel> clickSubject) {
            super(itemView);
            this.imageLoader = imageLoader;
            this.clickSubject = clickSubject;

            ButterKnife.bind(this, itemView);
        }

        public void setItem(final MovieViewModel movieViewModel) {
            this.movieViewModel = movieViewModel;
            imageLoader.loadImage(movieViewModel.getPosterPath(), moviePoster);
        }

        @OnClick(R.id.iv_main_movie_poster)
        void onMoviePosterClick() {
            clickSubject.onNext(movieViewModel);
        }
    }
}
