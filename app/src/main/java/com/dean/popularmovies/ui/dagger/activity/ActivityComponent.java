package com.dean.popularmovies.ui.dagger.activity;

import com.dean.popularmovies.ui.dagger.activity.module.ActivityModule;
import com.dean.popularmovies.ui.dagger.activity.module.ActivityPresenterModule;
import com.dean.popularmovies.ui.dagger.application.ApplicationComponent;

import dagger.Component;

@ActivityScope
@Component(
        dependencies = ApplicationComponent.class,
        modules = {
                ActivityModule.class,
                ActivityPresenterModule.class
        })
public interface ActivityComponent extends ActivityComponentExposes, ActivityComponentInjects {

    final class Initializer {

        static public ActivityComponent init(final DaggerActivity daggerActivity, final ApplicationComponent applicationComponent) {
            return DaggerActivityComponent.builder()
                    .applicationComponent(applicationComponent)
                    .activityModule(new ActivityModule(daggerActivity))
                    .activityPresenterModule(new ActivityPresenterModule(daggerActivity))
                    .build();
        }

        private Initializer() {
        }
    }
}
