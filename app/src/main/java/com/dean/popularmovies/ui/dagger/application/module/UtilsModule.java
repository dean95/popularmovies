package com.dean.popularmovies.ui.dagger.application.module;

import android.content.Context;

import com.dean.popularmovies.ui.dagger.application.ForApplication;
import com.dean.popularmovies.ui.util.ImageLoader;
import com.dean.popularmovies.ui.util.ImageLoaderImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class UtilsModule {

    @Provides
    @Singleton
    ImageLoader provideImageLoader(final @ForApplication Context context) {
        return new ImageLoaderImpl(context);
    }

    public interface Exposes {

        ImageLoader imageLoader();
    }
}
