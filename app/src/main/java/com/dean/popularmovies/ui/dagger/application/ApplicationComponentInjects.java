package com.dean.popularmovies.ui.dagger.application;

public interface ApplicationComponentInjects {

    void inject(PopularMoviesApplication popularMoviesApplication);
}
