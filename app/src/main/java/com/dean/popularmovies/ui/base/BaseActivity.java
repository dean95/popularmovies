package com.dean.popularmovies.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.dean.popularmovies.ui.dagger.activity.DaggerActivity;

public abstract class BaseActivity extends DaggerActivity implements BaseView {

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPresenter().start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPresenter().activate();
    }

    @Override
    protected void onPause() {
        super.onPause();
        getPresenter().deactivate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isFinishing()) {
            getPresenter().destroy();
        }
    }

    protected abstract ScopedPresenter getPresenter();
}
