package com.dean.popularmovies.ui.dagger.activity;

import javax.inject.Scope;

@Scope
public @interface ActivityScope {
}
