package com.dean.popularmovies.ui.detail;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dean.popularmovies.R;
import com.dean.popularmovies.ui.base.BaseActivity;
import com.dean.popularmovies.ui.base.ScopedPresenter;
import com.dean.popularmovies.ui.dagger.activity.ActivityComponent;
import com.dean.popularmovies.ui.model.MovieViewModel;
import com.dean.popularmovies.ui.model.ReviewViewModel;
import com.dean.popularmovies.ui.model.TrailerViewModel;
import com.dean.popularmovies.ui.util.ImageLoader;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public final class DetailActivity extends BaseActivity implements DetailContract.View {

    @Inject
    DetailContract.Presenter presenter;

    @Inject
    ImageLoader imageLoader;

    @Inject
    LayoutInflater layoutInflater;

    @Inject
    Resources resources;

    @BindView(R.id.iv_detail_poster)
    ImageView moviePoster;

    @BindView(R.id.tv_detail_title)
    TextView title;

    @BindView(R.id.tv_detail_release_date)
    TextView releaseDate;

    @BindView(R.id.tv_detail_rating)
    TextView rating;

    @BindView(R.id.tv_detail_description)
    TextView description;

    @BindView(R.id.tv_trailer_empty_view)
    TextView emptyTrailers;

    @BindView(R.id.rb_detail_rating)
    RatingBar ratingBar;

    @BindView(R.id.rv_detail_trailers)
    RecyclerView trailers;

    @BindView(R.id.movie_details_container)
    LinearLayout container;

    @BindView(R.id.iv_detail_favorite)
    ImageView favorite;

    public static final String MOVIE_EXTRA = "movie_extra";
    private static final String POPUP_SHOWN_KEY = "popup_shown_key";

    private DetailTrailersAdapter trailersAdapter;
    private DetailReviewsAdapter reviewsAdapter;

    private PopupWindow reviewsPopup;
    private TextView reviewsEmptyView;
    private RecyclerView reviewsList;

    private MovieViewModel movie;

    private boolean popupShown;
    private boolean isFavorite;

    public static Intent createIntent(final Activity activity) {
        return new Intent(activity, DetailActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        init(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(POPUP_SHOWN_KEY, popupShown);
        if (reviewsPopup != null && reviewsPopup.isShowing()) {
            reviewsPopup.dismiss();
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public ScopedPresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void inject(final ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    public void renderTrailers(List<TrailerViewModel> trailerViewModel) {
        trailersAdapter.onTrailersUpdate(trailerViewModel);
    }

    @Override
    public void renderReviews(List<ReviewViewModel> reviewViewModels) {
        if (reviewViewModels.isEmpty()) {
            showEmptyReviewView();
        } else {
            hideEmptyReviewsView();
        }
        reviewsAdapter.onReviewsUpdate(reviewViewModels);
    }

    @Override
    public void showEmptyTrailersView() {
        trailers.setVisibility(View.GONE);
        emptyTrailers.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyTrailersView() {
        emptyTrailers.setVisibility(View.GONE);
        trailers.setVisibility(View.VISIBLE);
    }

    @Override
    public void showEmptyReviewView() {
        reviewsList.setVisibility(View.GONE);
        reviewsEmptyView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyReviewsView() {
        reviewsEmptyView.setVisibility(View.GONE);
        reviewsList.setVisibility(View.VISIBLE);
    }

    @Override
    public void addFavoriteSuccess() {
        isFavorite = true;
        Toast.makeText(this, getString(R.string.details_movie_added_to_favorites_success_toast),
                Toast.LENGTH_SHORT).show();
        enableFavoriteStar();
    }

    @Override
    public void addFavoriteFail() {
        isFavorite = false;
        Toast.makeText(this, getString(R.string.details_movie_added_to_favorites_fail_toast),
                Toast.LENGTH_SHORT).show();
        disableFavoriteStar();
    }

    @Override
    public void setFavoriteTrue() {
        isFavorite = true;
        enableFavoriteStar();
    }

    @Override
    public void setFavoriteFalse() {
        isFavorite = false;
        disableFavoriteStar();
    }

    @Override
    public void favoriteRemoved() {
        isFavorite = false;
        Toast.makeText(this,
                resources.getString(R.string.details_movie_removed_from_favorites), Toast.LENGTH_SHORT).show();
        disableFavoriteStar();
    }

    @Override
    public void favoriteRemoveFail() {
        isFavorite = true;
        Toast.makeText(this,
                resources.getString(R.string.details_movie_remove_from_favorites_failed), Toast.LENGTH_SHORT).show();
        enableFavoriteStar();
    }

    @OnClick(R.id.btn_detail_show_review)
    public void onShowReviewClick() {
        initializeReviewsPopup();
    }

    @OnClick(R.id.iv_detail_favorite)
    public void onFavoriteClick() {
        if (isFavorite) {
            presenter.removeFromFavorites(movie.getId());
        } else {
            presenter.addMovieToFavorite(movie.getId(), movie.getTitle(), movie.getPosterPath(),
                    movie.getDescription(), movie.getRating(), movie.getReleaseDate());
        }
    }

    private void init(final Bundle savedInstanceState) {
        ButterKnife.bind(this);
        initializeRecyclerView();

        if (getIntent().getParcelableExtra(MOVIE_EXTRA) != null) {
            movie = getIntent().getParcelableExtra(MOVIE_EXTRA);
            renderMovie();
        }

        presenter.isFavorite(movie.getId());

        if (savedInstanceState != null) {
            popupShown = savedInstanceState.getBoolean(POPUP_SHOWN_KEY);
            if (popupShown) {
                container.post(this::initializeReviewsPopup);
            }
        }
    }

    private void renderMovie() {
        imageLoader.loadImage(movie.getPosterPath(), moviePoster);
        title.setText(movie.getTitle());
        releaseDate.setText(movie.getReleaseDate());
        ratingBar.setRating((float) movie.getRating());
        rating.setText(String.format("%s/10", String.valueOf(movie.getRating())));
        description.setText(movie.getDescription());

        presenter.getTrailers(movie.getId());
    }

    private void initializeRecyclerView() {
        final RecyclerView.LayoutManager layoutManager =
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        final DividerItemDecoration itemDecoration = new DividerItemDecoration(trailers.getContext(),
                DividerItemDecoration.VERTICAL);
        trailers.addItemDecoration(itemDecoration);

        if (trailersAdapter == null) {
            trailersAdapter = new DetailTrailersAdapter();
            trailers.setAdapter(trailersAdapter);
        } else {
            trailersAdapter = (DetailTrailersAdapter) trailers.getAdapter();
        }
        trailers.setLayoutManager(layoutManager);
        trailersAdapter.onItemClick().subscribe(this::onTrailerPlaySelected);

        trailers.setOnTouchListener((v, event) -> {
            v.getParent().requestDisallowInterceptTouchEvent(true);
            return false;
        });
    }

    private void onTrailerPlaySelected(final TrailerViewModel trailerViewModel) {
        presenter.showTrailer(trailerViewModel.getKey());
    }

    private void initializeReviewsPopup() {
        final Display display = getWindowManager().getDefaultDisplay();
        final Point size = new Point();

        display.getSize(size);

        final int width = size.x;
        final int margin = 120;

        final View reviewsLayout = layoutInflater.inflate(R.layout.reviews_layout, null);

        reviewsPopup = new PopupWindow(container.getContext());
        reviewsPopup.setContentView(reviewsLayout);
        reviewsPopup.setWidth(width - (2 * margin));
        reviewsPopup.setHeight(ViewGroup.LayoutParams.MATCH_PARENT);

        final ImageView dismissButton = reviewsLayout.findViewById(R.id.iv_detail_close_reviews);
        dismissButton.setOnClickListener(v -> {
            reviewsPopup.dismiss();
            popupShown = false;
        });

        reviewsList = reviewsLayout.findViewById(R.id.rv_detail_reviews);
        reviewsList.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        reviewsEmptyView = reviewsLayout.findViewById(R.id.reviews_empty_view);

        final DividerItemDecoration itemDecoration = new DividerItemDecoration(reviewsList.getContext(),
                DividerItemDecoration.VERTICAL);
        reviewsList.addItemDecoration(itemDecoration);

        reviewsAdapter = new DetailReviewsAdapter();
        reviewsList.setAdapter(reviewsAdapter);

        reviewsPopup.setAnimationStyle(R.style.popup_window_animation);
        reviewsPopup.showAtLocation(container, Gravity.CENTER, 0, 0);
        popupShown = true;

        presenter.getReviews(movie.getId());
    }

    private void enableFavoriteStar() {
        favorite.setImageDrawable(resources.getDrawable(R.drawable.ic_star_black_24dp));
    }

    private void disableFavoriteStar() {
        favorite.setImageDrawable(resources.getDrawable(R.drawable.ic_star_border_black_24dp));
    }
}
