package com.dean.popularmovies.ui.dagger.application;

import com.dean.popularmovies.ui.dagger.application.module.ApplicationModule;
import com.dean.popularmovies.ui.dagger.application.module.DataModule;
import com.dean.popularmovies.ui.dagger.application.module.MappersModule;
import com.dean.popularmovies.ui.dagger.application.module.ThreadingModule;
import com.dean.popularmovies.ui.dagger.application.module.UtilsModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {
                ApplicationModule.class,
                ThreadingModule.class,
                DataModule.class,
                MappersModule.class,
                UtilsModule.class
        })
public interface ApplicationComponent extends ApplicationComponentExposes, ApplicationComponentInjects {

    final class Initializer {

        static public ApplicationComponent init(final PopularMoviesApplication popularMoviesApplication) {
            return DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(popularMoviesApplication))
                    .threadingModule(new ThreadingModule())
                    .dataModule(new DataModule())
                    .mappersModule(new MappersModule())
                    .utilsModule(new UtilsModule())
                    .build();
        }

        private Initializer() {
        }
    }
}
