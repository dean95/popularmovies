package com.dean.popularmovies.ui.detail;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dean.popularmovies.R;
import com.dean.popularmovies.ui.model.ReviewViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class DetailReviewsAdapter extends RecyclerView.Adapter<DetailReviewsAdapter.TrailerViewHolder> {

    private List<ReviewViewModel> reviewViewModels = new ArrayList<>();

    @Override
    public TrailerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.review_item, parent, false);
        return new TrailerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TrailerViewHolder holder, int position) {
        holder.setItem(reviewViewModels.get(position));
    }

    @Override
    public int getItemCount() {
        return reviewViewModels.size();
    }

    public void onReviewsUpdate(final List<ReviewViewModel> reviewViewModels) {
        this.reviewViewModels = reviewViewModels;
        notifyDataSetChanged();
    }

    static final class TrailerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_detail_review_author)
        TextView author;

        @BindView(R.id.tv_detail_review_content)
        TextView content;

        private ReviewViewModel reviewViewModel;

        public TrailerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setItem(final ReviewViewModel reviewViewModel) {
            this.reviewViewModel = reviewViewModel;

            author.setText(reviewViewModel.getAuthor());
            content.setText(reviewViewModel.getContent());
        }
    }
}
