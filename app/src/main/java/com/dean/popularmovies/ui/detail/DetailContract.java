package com.dean.popularmovies.ui.detail;

import com.dean.popularmovies.ui.base.BaseView;
import com.dean.popularmovies.ui.base.ScopedPresenter;
import com.dean.popularmovies.ui.model.ReviewViewModel;
import com.dean.popularmovies.ui.model.TrailerViewModel;

import java.util.List;

public interface DetailContract {

    interface Presenter extends ScopedPresenter {

        void getTrailers(int id);

        void getReviews(int id);

        void showTrailer(String key);

        void addMovieToFavorite(int id, String title, String posterUrl, String overview,
                                double rating, String releaseDate);

        void isFavorite(int id);

        void removeFromFavorites(int id);
    }

    interface View extends BaseView {

        void renderTrailers(List<TrailerViewModel> trailerViewModel);

        void renderReviews(List<ReviewViewModel> reviewViewModels);

        void showEmptyTrailersView();

        void hideEmptyTrailersView();

        void showEmptyReviewView();

        void hideEmptyReviewsView();

        void addFavoriteSuccess();

        void addFavoriteFail();

        void setFavoriteTrue();

        void setFavoriteFalse();

        void favoriteRemoved();

        void favoriteRemoveFail();
    }
}
