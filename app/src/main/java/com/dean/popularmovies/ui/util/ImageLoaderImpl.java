package com.dean.popularmovies.ui.util;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public final class ImageLoaderImpl implements ImageLoader {

    private final Context context;

    public ImageLoaderImpl(final Context context) {
        this.context = context;
    }

    @Override
    public void loadImage(final String url, final ImageView targetView) {
        Picasso.with(context)
               .load(url)
               .into(targetView);
    }
}
