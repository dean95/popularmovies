package com.dean.popularmovies.ui.main;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dean.popularmovies.R;
import com.dean.popularmovies.ui.base.BaseActivity;
import com.dean.popularmovies.ui.base.ScopedPresenter;
import com.dean.popularmovies.ui.dagger.activity.ActivityComponent;
import com.dean.popularmovies.ui.model.MovieViewModel;
import com.dean.popularmovies.ui.util.ImageLoader;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class MainActivity extends BaseActivity implements MainContract.View {

    @Inject
    MainContract.Presenter presenter;

    @Inject
    ImageLoader imageLoader;

    @BindView(R.id.rv_main_movies)
    RecyclerView moviePosters;

    @BindView(R.id.tv_main_empty)
    TextView emptyText;

    @BindView(R.id.pb_main_loading_indicator)
    ProgressBar loadingIndicator;

    private static final int NUMBER_OF_COLUMNS = 2;

    private MainAdapter mainAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.getMovies();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();

        switch (itemId) {
            case R.id.main_menu_item_settings:
                presenter.showPreferences();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void renderMovies(final List<MovieViewModel> movieViewModels) {
        hideLoadingIndicator();
        hideEmptyView();
        mainAdapter.onMoviesUpdate(movieViewModels);
    }

    @Override
    public void movieFetchFailed() {
        hideLoadingIndicator();
        showEmptyView();
    }

    @Override
    protected ScopedPresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void inject(final ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    private void init() {
        ButterKnife.bind(this);
        initializeRecyclerView();
        showLoadingIndicator();
    }

    private void initializeRecyclerView() {
        final RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, NUMBER_OF_COLUMNS);

        if (moviePosters.getAdapter() == null) {
            mainAdapter = new MainAdapter(imageLoader);
            moviePosters.setAdapter(mainAdapter);
        } else {
            mainAdapter = (MainAdapter) moviePosters.getAdapter();
        }

        mainAdapter.onItemClick().subscribe(this::onPosterSelected);
        moviePosters.setLayoutManager(layoutManager);
        moviePosters.setHasFixedSize(true);
    }

    private void onPosterSelected(final MovieViewModel movieViewModel) {
        presenter.showDetails(movieViewModel);
    }

    private void showEmptyView() {
        moviePosters.setVisibility(View.GONE);
        emptyText.setVisibility(View.VISIBLE);
    }

    private void hideEmptyView() {
        emptyText.setVisibility(View.GONE);
        moviePosters.setVisibility(View.VISIBLE);
    }

    private void showLoadingIndicator() {
        moviePosters.setVisibility(View.GONE);
        loadingIndicator.setVisibility(View.VISIBLE);
    }

    private void hideLoadingIndicator() {
        loadingIndicator.setVisibility(View.GONE);
    }
}
