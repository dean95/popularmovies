package com.dean.popularmovies.data.movies.network.client;

import com.dean.popularmovies.data.movies.network.model.ApiResponse;
import com.dean.popularmovies.data.movies.network.model.ApiReview;
import com.dean.popularmovies.data.movies.network.model.ApiTrailer;

import io.reactivex.Single;

public final class TheMovieDbClientImpl implements TheMovieDbClient {

    private final TheMovieDbApi movieApi;

    public TheMovieDbClientImpl(final TheMovieDbApi movieApi) {
        this.movieApi = movieApi;
    }

    @Override
    public Single<ApiResponse> getPopularMovies() {
        return movieApi.getPopularMovies();
    }

    @Override
    public Single<ApiResponse> getTopRatedMovies() {
        return movieApi.getTopRatedMovies();
    }

    @Override
    public Single<ApiTrailer> getTrailers(int id) {
        return movieApi.getTrailers(id);
    }

    @Override
    public Single<ApiReview> getReviews(int id) {
        return movieApi.getReviews(id);
    }
}
