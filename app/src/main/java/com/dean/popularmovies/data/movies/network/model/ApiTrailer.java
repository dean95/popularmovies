package com.dean.popularmovies.data.movies.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class ApiTrailer {

    @SerializedName("id")
    private int id;

    @SerializedName("results")
    private List<ApiTrailerResults> trailerResults;

    public int getId() {
        return id;
    }

    public List<ApiTrailerResults> getTrailerResults() {
        return trailerResults;
    }
}
