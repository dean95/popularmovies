package com.dean.popularmovies.data.movies.network.client;

import com.dean.popularmovies.data.movies.network.model.ApiResponse;
import com.dean.popularmovies.data.movies.network.model.ApiReview;
import com.dean.popularmovies.data.movies.network.model.ApiTrailer;

import io.reactivex.Single;

public interface TheMovieDbClient {

    Single<ApiResponse> getPopularMovies();

    Single<ApiResponse> getTopRatedMovies();

    Single<ApiTrailer> getTrailers(int id);

    Single<ApiReview> getReviews(int id);
}
