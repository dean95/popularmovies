package com.dean.popularmovies.data.util;

public interface PreferenceUtils {

    String getPreferredCategory();
}
