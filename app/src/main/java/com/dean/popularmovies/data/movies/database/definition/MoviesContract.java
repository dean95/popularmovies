package com.dean.popularmovies.data.movies.database.definition;

import android.net.Uri;
import android.provider.BaseColumns;

public final class MoviesContract {

    public static final String AUTHORITY = "com.dean.popularmovies";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    public static final String PATH_FAVORITES = "favorites";

    public static final class FavoritesEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(PATH_FAVORITES).build();

        public static final String TABLE_NAME = "favorites";
        public static final String COLUMN_MOVIE_ID = "movie_id";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_POSTER_URL = "poster_url";
        public static final String COLUMN_OVERVIEW = "overview";
        public static final String COLUMN_RATING = "rating";
        public static final String COLUMN_RELEASE_DATE = "release_date";

        public static String createTableQuery() {
            return "CREATE TABLE " + TABLE_NAME + "(" +
                    _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_MOVIE_ID + " INTEGER NOT NULL, " +
                    COLUMN_TITLE + " TEXT NOT NULL, " +
                    COLUMN_POSTER_URL + " TEXT NOT NULL, " +
                    COLUMN_OVERVIEW + " TEXT NOT NULL, " +
                    COLUMN_RATING + " REAL NOT NULL, " +
                    COLUMN_RELEASE_DATE + " TEXT NOT NULL);";
        }

        public static String dropTableIfExists() {
            return "DROP TABLE IF EXISTS " + TABLE_NAME;
        }
    }
}
