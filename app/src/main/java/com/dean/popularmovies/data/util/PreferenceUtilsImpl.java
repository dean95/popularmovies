package com.dean.popularmovies.data.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;

import com.dean.popularmovies.R;

public final class PreferenceUtilsImpl implements PreferenceUtils {

    private final SharedPreferences preferences;
    private final Resources resources;

    public PreferenceUtilsImpl(final Context context, final Resources resources) {
        this.preferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.resources = resources;
    }

    @Override
    public String getPreferredCategory() {
        return preferences.getString(resources.getString(R.string.pref_movies_category),
                                     resources.getString(R.string.pref_movies_category_default));
    }
}
