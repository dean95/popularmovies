package com.dean.popularmovies.data.movies.converter;

import android.database.Cursor;

import com.dean.popularmovies.data.movies.database.definition.MoviesContract;
import com.dean.popularmovies.data.movies.network.model.ApiResponse;
import com.dean.popularmovies.data.movies.network.model.ApiResult;
import com.dean.popularmovies.data.movies.network.model.ApiReview;
import com.dean.popularmovies.data.movies.network.model.ApiReviewResults;
import com.dean.popularmovies.data.movies.network.model.ApiTrailer;
import com.dean.popularmovies.data.movies.network.model.ApiTrailerResults;
import com.dean.popularmovies.domain.model.Movie;
import com.dean.popularmovies.domain.model.Review;
import com.dean.popularmovies.domain.model.Trailer;

import java.util.ArrayList;
import java.util.List;

public final class MovieModelConverterImpl implements MovieModelConverter {

    private final String POSTER_BASE_URL = "http://image.tmdb.org/t/p/";
    private final String POSTER_SIZE = "w185";

    @Override
    public List<Movie> apiToDomain(final ApiResponse apiResponse) {
        final List<Movie> movies = new ArrayList<>(apiResponse.getResults().size());

        for (ApiResult result : apiResponse.getResults()) {
            final String posterPath = POSTER_BASE_URL + POSTER_SIZE + result.getPosterPath();
            final Movie movie = new Movie(result.getId(), result.getVoteAverage(), result.getTitle(),
                    posterPath, result.getOverview(), result.getReleaseDate());
            movies.add(movie);
        }

        return movies;
    }

    @Override
    public List<Trailer> apiTrailerToDomain(final ApiTrailer apiTrailer) {
        final List<Trailer> trailers = new ArrayList<>(apiTrailer.getTrailerResults().size());

        for (ApiTrailerResults result : apiTrailer.getTrailerResults()) {
            final Trailer trailer = new Trailer(result.getName(), result.getKey());
            trailers.add(trailer);
        }

        return trailers;
    }

    @Override
    public List<Review> apiReviewToDomain(final ApiReview apiReview) {
        final List<Review> reviews = new ArrayList<>(apiReview.getReviews().size());

        for (final ApiReviewResults result : apiReview.getReviews()) {
            final Review review = new Review(result.getAuthor(), result.getContent(), result.getId(),
                    result.getUrl());
            reviews.add(review);
        }

        return reviews;
    }

    @Override
    public List<Movie> dbToDomain(Cursor cursor) {
        final List<Movie> movies = new ArrayList<>(cursor.getCount());

        if (cursor.moveToFirst()) {
            do {
                final int id = cursor.getInt(cursor.getColumnIndex(MoviesContract.FavoritesEntry.COLUMN_MOVIE_ID));
                final double voteAverage = cursor.getDouble(cursor.getColumnIndex(MoviesContract.FavoritesEntry.COLUMN_RATING));
                final String title = cursor.getString(cursor.getColumnIndex(MoviesContract.FavoritesEntry.COLUMN_TITLE));
                final String posterUrl = cursor.getString(cursor.getColumnIndex(MoviesContract.FavoritesEntry.COLUMN_POSTER_URL));
                final String overview = cursor.getString(cursor.getColumnIndex(MoviesContract.FavoritesEntry.COLUMN_OVERVIEW));
                final String releaseDate = cursor.getString(cursor.getColumnIndex(MoviesContract.FavoritesEntry.COLUMN_RELEASE_DATE));

                final Movie movie = new Movie(id, voteAverage, title, posterUrl, overview, releaseDate);
                movies.add(movie);
            } while (cursor.moveToNext());
        }

        return movies;
    }
}
