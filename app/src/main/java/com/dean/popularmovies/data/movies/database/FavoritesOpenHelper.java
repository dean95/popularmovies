package com.dean.popularmovies.data.movies.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.dean.popularmovies.data.movies.database.definition.MoviesContract;

public final class FavoritesOpenHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "movies";
    private static final int DB_VERSION = 1;

    public FavoritesOpenHelper(final Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(MoviesContract.FavoritesEntry.createTableQuery());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(MoviesContract.FavoritesEntry.dropTableIfExists());
        onCreate(db);
    }
}
