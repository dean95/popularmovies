package com.dean.popularmovies.data.movies.network.client;

import com.dean.popularmovies.data.movies.network.model.ApiResponse;
import com.dean.popularmovies.data.movies.network.model.ApiReview;
import com.dean.popularmovies.data.movies.network.model.ApiTrailer;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface TheMovieDbApi {

    @GET("movie/popular?api_key=9ecc06003b1ab2b1d6b2f0d8e0058de0")
    Single<ApiResponse> getPopularMovies();

    @GET("movie/top_rated?api_key=9ecc06003b1ab2b1d6b2f0d8e0058de0")
    Single<ApiResponse> getTopRatedMovies();

    @GET("movie/{movieId}/videos?api_key=9ecc06003b1ab2b1d6b2f0d8e0058de0")
    Single<ApiTrailer> getTrailers(@Path("movieId") int movieId);

    @GET("movie/{movieId}/reviews?api_key=9ecc06003b1ab2b1d6b2f0d8e0058de0")
    Single<ApiReview> getReviews(@Path("movieId") int movieId);
}
