package com.dean.popularmovies.data.movies.database.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.dean.popularmovies.data.movies.database.FavoritesOpenHelper;
import com.dean.popularmovies.data.movies.database.definition.MoviesContract;

public final class FavoritesContentProvider extends ContentProvider {

    public static final int FAVORITES = 100;
    public static final int FAVORITE_WITH_ID = 101;

    private static final UriMatcher matcher = buildUriMatcher();

    private FavoritesOpenHelper favoritesDbHelper;

    private static UriMatcher buildUriMatcher() {
        final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        uriMatcher.addURI(MoviesContract.AUTHORITY, MoviesContract.PATH_FAVORITES, FAVORITES);
        uriMatcher.addURI(MoviesContract.AUTHORITY, MoviesContract.PATH_FAVORITES + "/#", FAVORITE_WITH_ID);

        return uriMatcher;
    }

    @Override
    public boolean onCreate() {
        favoritesDbHelper = new FavoritesOpenHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection,
                        @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        final SQLiteDatabase db = favoritesDbHelper.getReadableDatabase();
        final int match = matcher.match(uri);

        Cursor returnCursor;
        switch (match) {
            case FAVORITES:
                returnCursor = db.query(MoviesContract.FavoritesEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            case FAVORITE_WITH_ID:
                final String id = uri.getPathSegments().get(1);
                final String selectionCriteria = MoviesContract.FavoritesEntry.COLUMN_MOVIE_ID + "=?";
                final String[] selectionArguments = new String[]{id};

                returnCursor = db.query(MoviesContract.FavoritesEntry.TABLE_NAME,
                        projection,
                        selectionCriteria,
                        selectionArguments,
                        null,
                        null,
                        sortOrder
                );
                break;
            default:
                throw new UnsupportedOperationException();
        }

        returnCursor.setNotificationUri(getContext().getContentResolver(), uri);

        return returnCursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        final SQLiteDatabase db = favoritesDbHelper.getWritableDatabase();
        final int match = matcher.match(uri);

        final Uri returnUri;
        switch (match) {
            case FAVORITES:
                long id = db.insert(MoviesContract.FavoritesEntry.TABLE_NAME, null, values);
                if (id > 0) {
                    returnUri = ContentUris.withAppendedId(MoviesContract.FavoritesEntry.CONTENT_URI, id);
                } else {
                    throw new SQLException();
                }
                break;
            default:
                throw new UnsupportedOperationException("Unknown URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        final SQLiteDatabase db = favoritesDbHelper.getWritableDatabase();

        final int match = matcher.match(uri);

        int tasksDeleted;
        switch (match) {
            case FAVORITE_WITH_ID:
                final String id = uri.getPathSegments().get(1);
                final String whereClause = MoviesContract.FavoritesEntry.COLUMN_MOVIE_ID + "=?";
                tasksDeleted = db.delete(MoviesContract.FavoritesEntry.TABLE_NAME, whereClause, new String[]{id});
                break;
            default:
                throw new UnsupportedOperationException();
        }

        if (tasksDeleted > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return tasksDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
