package com.dean.popularmovies.data.movies.database.model;

public final class DbMovie {

    private final int id;
    private final String title;
    private final String posterUrl;
    private final String overview;
    private final double rating;
    private final String releaseDate;

    public DbMovie(int id, String title, String posterUrl, String overview, double rating, String releaseDate) {
        this.id = id;
        this.title = title;
        this.posterUrl = posterUrl;
        this.overview = overview;
        this.rating = rating;
        this.releaseDate = releaseDate;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public String getOverview() {
        return overview;
    }

    public double getRating() {
        return rating;
    }

    public String getReleaseDate() {
        return releaseDate;
    }
}
