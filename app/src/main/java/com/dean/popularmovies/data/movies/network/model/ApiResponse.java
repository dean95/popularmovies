package com.dean.popularmovies.data.movies.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class ApiResponse {

    @SerializedName("page")
    private int page;

    @SerializedName("total_results")
    private int totalResults;

    @SerializedName("total_pages")
    private int totalPages;

    @SerializedName("results")
    private List<ApiResult> results;

    public int getPage() {
        return page;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public List<ApiResult> getResults() {
        return results;
    }
}
