package com.dean.popularmovies.data.movies;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import com.dean.popularmovies.data.movies.converter.MovieModelConverter;
import com.dean.popularmovies.data.movies.database.definition.MoviesContract;
import com.dean.popularmovies.data.movies.network.client.TheMovieDbClient;
import com.dean.popularmovies.data.util.PreferenceUtils;
import com.dean.popularmovies.domain.model.Movie;
import com.dean.popularmovies.domain.model.Review;
import com.dean.popularmovies.domain.model.Trailer;
import com.dean.popularmovies.domain.repository.MovieRepository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Scheduler;
import io.reactivex.Single;

public final class MovieRepositoryImpl implements MovieRepository {

    private final String POPULAR_MOVIES_VALUE = "popular";
    private final String TOP_RATED_MOVIES_VALUE = "top_rated";
    private final String FAVORITE_MOVIES_VALUE = "favorite";

    private final TheMovieDbClient movieClient;
    private final MovieModelConverter movieModelConverter;
    private final Scheduler backgroundScheduler;
    private final PreferenceUtils preferenceUtils;
    private final ContentResolver contentResolver;

    public MovieRepositoryImpl(final TheMovieDbClient movieClient, final MovieModelConverter movieModelConverter,
                               final Scheduler backgroundScheduler, final PreferenceUtils preferenceUtils,
                               final ContentResolver contentResolver) {
        this.movieClient = movieClient;
        this.movieModelConverter = movieModelConverter;
        this.backgroundScheduler = backgroundScheduler;
        this.preferenceUtils = preferenceUtils;
        this.contentResolver = contentResolver;
    }

    @Override
    public Single<List<Movie>> getMovies() {
        return Single.fromCallable(preferenceUtils::getPreferredCategory)
                .flatMap(category -> {
                    switch (category) {
                        case POPULAR_MOVIES_VALUE:
                            return getPopularMovies();
                        case TOP_RATED_MOVIES_VALUE:
                            return getTopRatedMovies();
                        case FAVORITE_MOVIES_VALUE:
                            return getFavoriteMovies();
                        default:
                            return Single.error(new Throwable("Error"));
                    }
                })
                .subscribeOn(backgroundScheduler);
    }

    @Override
    public Single<List<Trailer>> getTrailers(final int id) {
        return movieClient.getTrailers(id)
                .map(movieModelConverter::apiTrailerToDomain)
                .subscribeOn(backgroundScheduler);
    }

    @Override
    public Single<List<Review>> getReviews(final int id) {
        return movieClient.getReviews(id)
                .map(movieModelConverter::apiReviewToDomain)
                .subscribeOn(backgroundScheduler);
    }

    @Override
    public Completable addMovieToFavorite(int id, String title, String posterUrl, String overview,
                                          double rating, String releaseDate) {
        return Completable.fromCallable(() -> {
            addMovieToFavorites(id, title, posterUrl, overview, rating, releaseDate);
            return null;
        }).subscribeOn(backgroundScheduler);
    }

    @Override
    public Maybe<Cursor> isFavorite(int id) {
        return Maybe.fromCallable(() -> contentResolver.query(
                Uri.withAppendedPath(MoviesContract.FavoritesEntry.CONTENT_URI, String.valueOf(id)),
                null, null, null, null))
                .subscribeOn(backgroundScheduler);
    }

    @Override
    public Completable removeFromFavorites(int id) {
        return Completable.fromCallable(() ->
                contentResolver.delete(
                        Uri.withAppendedPath(MoviesContract.FavoritesEntry.CONTENT_URI, String.valueOf(id)), null, null))
                .subscribeOn(backgroundScheduler);
    }

    private void addMovieToFavorites(int id, String title, String posterUrl, String overview,
                                     double rating, String releaseDate) {
        final ContentValues contentValues = new ContentValues();
        contentValues.put(MoviesContract.FavoritesEntry.COLUMN_MOVIE_ID, id);
        contentValues.put(MoviesContract.FavoritesEntry.COLUMN_TITLE, title);
        contentValues.put(MoviesContract.FavoritesEntry.COLUMN_POSTER_URL, posterUrl);
        contentValues.put(MoviesContract.FavoritesEntry.COLUMN_OVERVIEW, overview);
        contentValues.put(MoviesContract.FavoritesEntry.COLUMN_RATING, rating);
        contentValues.put(MoviesContract.FavoritesEntry.COLUMN_RELEASE_DATE, releaseDate);

        contentResolver.insert(MoviesContract.FavoritesEntry.CONTENT_URI, contentValues);
    }

    private Single<List<Movie>> getPopularMovies() {
        return movieClient.getPopularMovies()
                .map(movieModelConverter::apiToDomain);
    }

    private Single<List<Movie>> getTopRatedMovies() {
        return movieClient.getTopRatedMovies()
                .map(movieModelConverter::apiToDomain);
    }

    private Single<List<Movie>> getFavoriteMovies() {
        return Single.fromCallable(
                () -> contentResolver.query(MoviesContract.FavoritesEntry.CONTENT_URI,
                        null, null, null, null))
                .map(movieModelConverter::dbToDomain);
    }
}
