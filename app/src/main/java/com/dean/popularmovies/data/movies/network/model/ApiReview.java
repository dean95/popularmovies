package com.dean.popularmovies.data.movies.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class ApiReview {

    @SerializedName("id")
    private int id;

    @SerializedName("page")
    private int page;

    @SerializedName("results")
    private List<ApiReviewResults> reviews;

    public ApiReview(int id, int page, List<ApiReviewResults> reviews) {

        this.id = id;
        this.page = page;
        this.reviews = reviews;
    }

    public int getId() {
        return id;
    }

    public int getPage() {
        return page;
    }

    public List<ApiReviewResults> getReviews() {
        return reviews;
    }
}
