package com.dean.popularmovies.data.movies.converter;

import android.database.Cursor;

import com.dean.popularmovies.data.movies.network.model.ApiResponse;
import com.dean.popularmovies.data.movies.network.model.ApiReview;
import com.dean.popularmovies.data.movies.network.model.ApiTrailer;
import com.dean.popularmovies.domain.model.Movie;
import com.dean.popularmovies.domain.model.Review;
import com.dean.popularmovies.domain.model.Trailer;

import java.util.List;

public interface MovieModelConverter {

    List<Movie> apiToDomain(ApiResponse apiResponse);

    List<Trailer> apiTrailerToDomain(ApiTrailer apiTrailer);

    List<Review> apiReviewToDomain(ApiReview apiReview);

    List<Movie> dbToDomain(Cursor cursor);
}
