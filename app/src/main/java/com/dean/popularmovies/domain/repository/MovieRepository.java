package com.dean.popularmovies.domain.repository;

import android.database.Cursor;

import com.dean.popularmovies.domain.model.Movie;
import com.dean.popularmovies.domain.model.Review;
import com.dean.popularmovies.domain.model.Trailer;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;

public interface MovieRepository {

    Single<List<Movie>> getMovies();

    Single<List<Trailer>> getTrailers(int id);

    Single<List<Review>> getReviews(int id);

    Completable addMovieToFavorite(int id, String title, String posterUrl, String overview,
                                   double rating, String releaseDate);

    Maybe<Cursor> isFavorite(int id);

    Completable removeFromFavorites(int id);
}
