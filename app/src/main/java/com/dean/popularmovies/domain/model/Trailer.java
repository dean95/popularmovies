package com.dean.popularmovies.domain.model;

public final class Trailer {

    private String name;
    private String key;

    public Trailer(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }
}
