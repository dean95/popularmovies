package com.dean.popularmovies.domain.model;

public final class Movie {

    private final int id;
    private final double voteAverage;
    private final String title;
    private final String posterPath;
    private final String overview;
    private final String releaseDate;

    public Movie(final int id, final double voteAverage, final String title,
                 final String posterPath, final String overview, final String releaseDate) {
        this.id = id;
        this.voteAverage = voteAverage;
        this.title = title;
        this.posterPath = posterPath;
        this.overview = overview;
        this.releaseDate = releaseDate;
    }

    public int getId() {
        return id;
    }

    public double getVoteAverage() {
        return voteAverage;
    }

    public String getTitle() {
        return title;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public String getOverview() {
        return overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }
}
